export APP_PWD=foobarbaz

db() {
  docker run -P --volumes-from app_data --name app_db -e POSTGRES_USER=app_user -e POSTGRES_PASSWORD=$APP_PWD -d postgres:latest
}

app() {
  docker run -p 80:80 --link app_db:postgres --name app hgammon/app
}

action=$1

${action}
